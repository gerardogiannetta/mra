package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MarsRover {

	private int planetX;
	private int planetY;
	private List<String> planetObstacles;
	private CurrentPosition currentPosition;
	private static final String COMMA = ",";
	private static final String LANDING_STATUS = "(0,0,N)";
	private List<String> obstacles;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		if (planetX < 0 || planetY < 0) {
			throw new MarsRoverException("invalid x or y");
		}
		this.planetX = planetX;
		this.planetY = planetY;
		checkPlanetObstaclesRegex(planetObstacles);
		this.planetObstacles = planetObstacles;
		
		this.currentPosition = new CurrentPosition(planetX, planetY);
		obstacles = new ArrayList<>();
	}
	
	
	private void checkPlanetObstaclesRegex(List<String> planetObstacles) throws MarsRoverException {
		Pattern p = Pattern.compile("\\([0-9]+,[0-9]+\\)");
		for (String po : planetObstacles) {
			Matcher m = p.matcher(po);
			if (! m.matches()) {
				throw new MarsRoverException("the obstacles are bad written");
			}
		}
	}
	

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if (x < 0 || x >= planetX || y < 0 || y >= planetY) {
			throw new MarsRoverException("X or Y is invalid");
		}
		
		for (String ob : planetObstacles) {
			StringTokenizer xy = deleteParenthesisAndTokenizeString(ob);
			
			int xx = Integer.parseInt(xy.nextToken());
			int yy = Integer.parseInt(xy.nextToken());
			
			if (x == xx && y == yy) {
				return true;
			}
		}
		return false;
	}
	
	
	private StringTokenizer deleteParenthesisAndTokenizeString(String ob) {
		String noParenthesis = ob.substring(1, ob.length()-1);
		return new StringTokenizer(noParenthesis, COMMA);
	}
	

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		if (commandString.length() > 1) {
			String[] arr = commandString.split("");
			for (int i = 0; i < arr.length-1; i++) {
				executeCommand(arr[i]);
			}
			return executeCommand(arr[arr.length-1]) + createStringObstacles(obstacles);
		}
		
		// rotate
		if (commandString.equals("r")) {
			rotateDirRight();
			return currentPosition.getPosition();
		}
		else if (commandString.equals("l")) {
			rotateDirLeft();
			return currentPosition.getPosition();
		}
		
		// move
		else if (commandString.equals("f")) {
			moveForward();
			return currentPosition.getPosition();
		}
		else if (commandString.equals("b")) {
			moveBackward();
			return currentPosition.getPosition();
		}
		
		return LANDING_STATUS;
	}
	
	
	private String createStringObstacles(List<String> ob) {
		String res = "";
		for (String o : ob) {
			res += o;
		}
		return res;
	}
	
	
	private void rotateDirRight() {
		String curDir = currentPosition.getCurrentDir();
		if (curDir.equals("N")) {
			currentPosition.setCurrentDir("E");
		}
		else if (curDir.equals("E")) {
			currentPosition.setCurrentDir("S");
		}
		else if (curDir.equals("S")) {
			currentPosition.setCurrentDir("W");
		}
		else if (curDir.equals("W")) {
			currentPosition.setCurrentDir("N");
		}
	}
	
	
	private void rotateDirLeft() {
		String curDir = currentPosition.getCurrentDir();
		if (curDir.equals("N")) {
			currentPosition.setCurrentDir("W");
		}
		else if (curDir.equals("E")) {
			currentPosition.setCurrentDir("N");
		}
		else if (curDir.equals("S")) {
			currentPosition.setCurrentDir("E");
		}
		else if (curDir.equals("W")) {
			currentPosition.setCurrentDir("S");
		}
	}
	
	
	private void moveForward() throws MarsRoverException {
		String curDir = currentPosition.getCurrentDir();
		
		if (curDir.equals("N") && !checkObstaclesAtNextY(+1)) {
			currentPosition.setCurrentY(currentPosition.getCurrentY()+1);
		}
		else if (curDir.equals("S") && !checkObstaclesAtNextY(+1)) {
			currentPosition.setCurrentY(currentPosition.getCurrentY()-1);
		}
		
		else if (curDir.equals("W") && !checkObstaclesAtNextX(-1)) {
			currentPosition.setCurrentX(currentPosition.getCurrentX()-1);
		}
		else if (curDir.equals("E") && !checkObstaclesAtNextX(+1)) {
			currentPosition.setCurrentX(currentPosition.getCurrentX()+1);
		}
		
	}
	
	
	private void moveBackward() throws MarsRoverException {
		String curDir = currentPosition.getCurrentDir();
		
		if (curDir.equals("N") && !checkObstaclesAtNextY(-1)) {
			currentPosition.setCurrentY(currentPosition.getCurrentY()-1);
		}
		else if (curDir.equals("S") && !checkObstaclesAtNextY(+1)) {
			currentPosition.setCurrentY(currentPosition.getCurrentY()+1);
		}
		
		else if (curDir.equals("W") && !checkObstaclesAtNextX(+1)) {
			currentPosition.setCurrentX(currentPosition.getCurrentX()+1);
		}
		else if (curDir.equals("E") && !checkObstaclesAtNextX(-1)) {
			currentPosition.setCurrentX(currentPosition.getCurrentX()-1);
		}
	}
	
	
	private boolean checkObstaclesAtNextY(int step) throws MarsRoverException {
		int nextY = ((((currentPosition.getCurrentY()+step)%planetY)+planetY)%planetY);
		int curX = currentPosition.getCurrentY();
		if (planetContainsObstacleAt(curX, nextY)) {
			String res = "("+curX+","+nextY+")";
			if (! searchObstacle(res)) {
				obstacles.add(res);
			}
			return true;
		}
		return false;
	}
	
	
	private boolean checkObstaclesAtNextX(int step) throws MarsRoverException {
		int nextX = ((((currentPosition.getCurrentX()+step)%planetX)+planetX)%planetX);
		int curY = currentPosition.getCurrentY();
		if (planetContainsObstacleAt(nextX, curY)) {
			String res = "("+nextX+","+curY+")";
			if (! searchObstacle(res)) {
				obstacles.add(res);
			}
			return true;
		}
		return false;
	}
	
	
	private boolean searchObstacle(String ob) {
		for (String o : obstacles) {
			if (o.equals(ob)) {
				return true;
			}
		}
		return false;
	}

}
