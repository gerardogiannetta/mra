package tdd.training.mra;

public class CurrentPosition {
	private int currentX;
	private int currentY;
	private String currentDir;
	private int planetX;
	private int planetY;

	public CurrentPosition(int planetX, int planetY) {
		this.currentX = 0;
		this.currentY = 0;
		this.currentDir = "N";
		this.planetX = planetX;
		this.planetY = planetY;
	}

	public int getCurrentX() {
		return currentX;
	}

	public void setCurrentX(int currentX) {
		this.currentX = (((currentX % planetX)+planetX)%planetX);
	}

	public int getCurrentY() {
		return currentY;
	}

	public void setCurrentY(int currentY) {
		this.currentY = (((currentY % planetY)+planetY)%planetY);
	}

	public String getCurrentDir() {
		return currentDir;
	}

	public void setCurrentDir(String currentDir) {
		this.currentDir = currentDir;
	}

	public String getPosition() {
		return "("+currentX+","+currentY+","+currentDir+")";
	}
}