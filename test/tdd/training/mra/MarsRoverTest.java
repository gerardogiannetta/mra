package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testPlanetContainsObstacleAtX0Y1() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(15,15)");
		planetObstacles.add("(2,2)");
		MarsRover mr = new MarsRover(20, 20, planetObstacles);
		assertTrue(mr.planetContainsObstacleAt(15, 15));
	}
	
	
	@Test
	public void testPlanetContainsObstacleAtX2Y2() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(19,1)");
		planetObstacles.add("(2,2)");
		MarsRover mr = new MarsRover(20, 20, planetObstacles);
		assertTrue(mr.planetContainsObstacleAt(2, 2));
	}
	
	
	@Test
	public void testPlanetNotContainsObstacleAtX1Y2() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover mr = new MarsRover(3, 3, planetObstacles);
		assertFalse(mr.planetContainsObstacleAt(1, 2));
	}
	
	
	@Test(expected = MarsRoverException.class)
	public void testPlanetContainsObstacleAtInvalidIndexesShouldReturnMarsRoverException() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover mr = new MarsRover(3, 3, planetObstacles);
		mr.planetContainsObstacleAt(10, 2);
	}
	
	
	@Test(expected = MarsRoverException.class)
	public void testInvalidPlanetObstaclesWithNoParenthesisShouldReturnMarsRoverException() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("2,2");
		new MarsRover(3, 3, planetObstacles);
	}
	
	
	@Test(expected = MarsRoverException.class)
	public void testInvalidPlanetObstaclesWithNoCommaBetweenNumbersShouldReturnMarsRoverException() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(22)");
		new MarsRover(3, 3, planetObstacles);
	}
	
	
	@Test(expected = MarsRoverException.class)
	public void testInvalidPlanetWithNegativeDimensionAtXShouldReturnMarsRoverException() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		new MarsRover(-1, 3, planetObstacles);
	}
	
	
	@Test
	public void testExecuteCommandWithEmptyStringShoudlReturnLandingStatus() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover mr = new MarsRover(3, 3, planetObstacles);
		assertEquals("(0,0,N)", mr.executeCommand(""));
	}
	
	
	@Test
	public void testExecuteCommandRShouldReturn00E() throws Exception {
		MarsRover mr = new MarsRover(3, 3, new ArrayList<>());
		assertEquals("(0,0,E)", mr.executeCommand("r"));
	}
	
	
	@Test
	public void testExecuteCommandLShouldReturn00W() throws Exception {
		MarsRover mr = new MarsRover(3, 3, new ArrayList<>());
		assertEquals("(0,0,W)", mr.executeCommand("l"));
	}
	
	
	@Test
	public void testExecuteCommandFShouldMovesForwardOfOneCellAndReturn01N() throws Exception {
		MarsRover mr = new MarsRover(3, 3, new ArrayList<>());
		assertEquals("(0,1,N)", mr.executeCommand("f"));
	}
	
	
	@Test
	public void testExecuteCommandBAndWithCommandFRFFShouldReturn21E() throws Exception {
		MarsRover mr = new MarsRover(3, 3, new ArrayList<>());
		mr.executeCommand("f");
		mr.executeCommand("r");
		mr.executeCommand("f");
		assertEquals("(2,1,E)", mr.executeCommand("f"));
	}
	
	
	@Test
	public void testExecuteCommandBAndWithCommandFRFFAndThanBShouldReturn11E() throws Exception {
		MarsRover mr = new MarsRover(3, 3, new ArrayList<>());
		mr.executeCommand("f");
		mr.executeCommand("r");
		mr.executeCommand("f");
		mr.executeCommand("f");
		assertEquals("(1,1,E)", mr.executeCommand("b"));
	}
	
	
	@Test
	public void testExecuteCommandBAndWithCommandFRFFBLAndThanBShouldReturn10N() throws Exception {
		MarsRover mr = new MarsRover(3, 3, new ArrayList<>());
		mr.executeCommand("f");
		mr.executeCommand("r");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("b");
		mr.executeCommand("l");
		assertEquals("(1,0,N)", mr.executeCommand("b"));
	}
	
	
	@Test
	public void testExecuteCommandBAndWithCommandFRFFBLBAndThanLShouldReturn10W() throws Exception {
		MarsRover mr = new MarsRover(3, 3, new ArrayList<>());
		mr.executeCommand("f");
		mr.executeCommand("r");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("b");
		mr.executeCommand("l");
		mr.executeCommand("b");
		assertEquals("(1,0,W)", mr.executeCommand("l"));
	}
	
	
	@Test
	public void testExecuteCombinedCommandAndShouldReturn10W() throws Exception {
		MarsRover mr = new MarsRover(3, 3, new ArrayList<>());
		assertEquals("(1,0,W)", mr.executeCommand("frffblbl"));
	}
	
	
	@Test
	public void testWrappingExecuteCommandBShoudlReturn09N() throws Exception {
		MarsRover mr = new MarsRover(10, 10, new ArrayList<>());
		assertEquals("(0,9,N)", mr.executeCommand("b"));
	}
	
	
	@Test
	public void testWrappingExecuteCommandBShoudlReturn00N() throws Exception {
		MarsRover mr = new MarsRover(3, 3, new ArrayList<>());
		assertEquals("(0,0,N)", mr.executeCommand("fff"));
	}
	
	
	@Test
	public void testExecuteCommandWithSingleObstacleInX2Y2ShouldReturn12Eand22() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover mr = new MarsRover(10, 10, planetObstacles);
		assertEquals("(1,2,E)(2,2)", mr.executeCommand("ffrfff"));
	}

}
