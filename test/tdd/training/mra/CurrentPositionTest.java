package tdd.training.mra;

import static org.junit.Assert.*;

import org.junit.Test;

public class CurrentPositionTest {

	@Test
	public void testCurrentPositionShouldReturnLandingStatusWhenCreated() throws Exception {
		CurrentPosition cp = new CurrentPosition(3, 3);
		assertEquals("(0,0,N)", cp.getPosition());
	}

}
